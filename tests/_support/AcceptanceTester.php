<?php
use Codeception\Util\Locator;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

   /**
    * Define custom actions here
    */
public function auth() {

$this->wantTo('go to the administrative panel and check access to all sections');
$this->amOnPage('/login');
$this->waitForElement(Locator::contains('button', 'Войти'), 10); // sec
$this->fillField('login','sbs-support');
$this->fillField('password','kZphLnlPZWhBlsb');
$this->click('Войти');
$this->wait('5');
}

}
